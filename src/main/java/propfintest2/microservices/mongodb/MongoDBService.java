package propfintest2.microservices.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ParallelScanOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.client.model.Updates;

// Indexes
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Filters;


import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class MongoDBService {

	static public void main(String[] args) {
		// To directly connect to a single MongoDB server (note that this will not auto-discover the primary even
		// if it's a member of a replica set:
		//MongoClient mongoClient = new MongoClient("cloud.mongodb.com");
		// or
		//MongoClient mongoClient = new MongoClient( "localhost" );
		// or
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		// or, to connect to a replica set, with auto-discovery of the primary, supply a seed list of members
		/*
		MongoClient mongoClient = new MongoClient(Arrays.asList(new ServerAddress("localhost", 27017),
		                                      new ServerAddress("localhost", 27018),
		                                      new ServerAddress("localhost", 27019)));
		*/
		//DB db = mongoClient.getDB( "mydb" );	
		
		//MongoDatabase db= mongoClient.getDatabase("propfintest");
		
		//System.out.println("db = " + db);
		//MongoCollection<Document> coll = db.getCollection("testCollection");
		BasicDBObject doc = new BasicDBObject("name", "MongoDB")
		        .append("type", "database")
		        .append("count", 1)
		        .append("info", new BasicDBObject("x", 203).append("y", 102));
		Document doc2 = new Document("name", "MongoDB");
		
		
		// Atlas MongoDB
		/*
		MongoClientURI uri = new MongoClientURI("mongodb://admin:propfin123@cluster0-shard-00-00-8bu2n.mongodb.net:27017,cluster0-shard-00-01-8bu2n.mongodb.net:27017,cluster0-shard-00-02-8bu2n.mongodb.net:27017/admin?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");
				   //"mongodb://admin:propfin123@cluster0-shard-00-00-8bu2n.mongodb.net:27017,mycluster0-shard-00-01-wpeiv.mongodb.net:27017,mycluster0-shard-00-02-wpeiv.mongodb.net:27017/admin?ssl=true&replicaSet=Mycluster0-shard-0&authSource=admin");
		
		MongoClient mongoClient2 = new MongoClient(uri);
		System.out.println("mongoClient2 = " + mongoClient2);
		*/
		
		//MongoDatabase database = mongoClient2.getDatabase("test");
		MongoDatabase database = mongoClient.getDatabase("test");
		
		//database.createCollection("testCollection");
		MongoCollection<Document> testCollection = database.getCollection("testCollection");
		System.out.println("database = " + database);
		MongoIterable<String> mongoIT = database.listCollectionNames();
		System.out.println("mongoIT = " + mongoIT);
		MongoCursor<String> mongoCursor = mongoIT.iterator();
		System.out.println("mongoCursor = " + mongoCursor);
		String firstName = mongoCursor.next();
		System.out.println("firstName = " + firstName);
		testCollection.insertOne(doc2);
		//coll.count();
		//coll.insertOne(doc2);
		//FindIterable<Document> documentList = coll.find();
		//System.out.println("documentList = " + documentList);

		Document doc3 = new Document("name1", "MongoDB")
				//.append("_id", "311111")
                .append("type", "database")
                .append("count", 1)
                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
                .append("info", new Document("x", 203).append("y", 102));
		
		testCollection.insertOne(doc3);
		
		List<Document> documents = new ArrayList<Document>();
		for (int i = 0; i < 100; i++) {
		    documents.add(new Document("i", i));
		}
		testCollection.insertMany(documents);
		System.out.println("testCollection.count = " + testCollection.count());

		Document myDoc = testCollection.find().first();
		System.out.println(myDoc.toJson());
		
		MongoCursor<Document> cursor = testCollection.find().iterator();
		try {
		    while (cursor.hasNext()) {
		        System.out.println(cursor.next().toJson());
		    }
		} finally {
		    cursor.close();
		}
		
		myDoc = testCollection.find(Filters.eq("i", 71)).first();
		System.out.println(myDoc.toJson());
		
		Block<Document> printBlock = new Block<Document>() {
		     
		     public void apply(final Document document) {
		         System.out.println(document.toJson());
		     }
		};

		testCollection.find(Filters.gt("i", 50)).forEach(printBlock);
		
		testCollection.find(Filters.and(Filters.gt("i", 50), Filters.lte("i", 100))).forEach(printBlock);
		
		testCollection.updateOne(Filters.eq("i", 10), new Document("$set", new Document("i", 110)));

		UpdateResult updateResult = testCollection.updateMany(Filters.lt("i", 100), Updates.inc("i", 100));
		System.out.println(updateResult.getModifiedCount());
		
		testCollection.deleteOne(Filters.eq("i", 110));

		DeleteResult deleteResult = testCollection.deleteMany(Filters.gte("i", 100));
		System.out.println(deleteResult.getDeletedCount());
		
		testCollection.createIndex(Indexes.ascending("name123"));

		/*
		Document doc4 = new Document("name1", "MongoDB")
				.append("_id", "4")
                .append("type", "database")
                .append("count", 1)
                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
                .append("info", new Document("x", 203).append("y", 102));
		
		testCollection.insertOne(doc4);
		*/
		
		
		
		Document doc5 = new Document("name1", "MongoDB")
				.append("_id", "311111")
                .append("type", "database")
                .append("count", 1)
                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
                .append("info", new Document("x", 203).append("y", 102));
		
		MongoCollection<Document> testCollection2 = database.getCollection("testCollection2");
		
		
		String hex = "0x11";
		ObjectId y = new ObjectId("599fe3b7ae1c7524c49ddd98");
		Document doc6 = new Document("_id", "31").append("rid", y);
		//testCollection2.insertOne(doc6);
		
		
		Document doc7 = testCollection2.find(Filters.eq("_id", "2")).first();
		System.out.println("doc7 = " + doc7);
		Document doc8 = (Document) doc7.get("rid");
		System.out.println("doc8 = " + doc8);
		String oid = doc8.getString("oid");
		System.out.println("oid = " + oid);
		Document doc9 = testCollection.find(Filters.eq("_id", oid)).first();
		System.out.println("doc9 = " + doc9);
		

		Document doc71 = testCollection2.find(Filters.eq("_id", "31")).first();
		System.out.println("doc71 = " + doc71);
		ObjectId objId = doc71.getObjectId("rid");
		System.out.println("objId = " + objId.toString());
		System.out.println(y.equals(objId));
		Document doc91 = testCollection.find(Filters.eq("_id", objId)).first();
		System.out.println("doc91 = " + doc91);
		
		Map<String, Object> mapDoc = new HashMap<String, Object>();
		mapDoc.put("id", "1001");
		mapDoc.put("y", y);
		mapDoc.put("doc", doc6);
		Document doc12 = new Document(mapDoc);
		testCollection2.insertOne(doc12);
		
		MongoCursor<Document> cursor2 = testCollection2.find().iterator();
		try {
		    while (cursor2.hasNext()) {
		        System.out.println(cursor2.next().toJson());
		    }
		} finally {
		    cursor2.close();
		}

	} // main
	
} // class MongoDBService

